// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BackendSwift",
    dependencies: [
        .package(url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git", from: "3.0.0"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-PostgreSQL.git", from: "3.0.0"),
        .package(url: "https://github.com/SwiftORM/Postgres-StORM.git", from: "3.0.0"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-Session.git", from: "3.0.0"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-Session-PostgreSQL.git", from: "3.0.0"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-LocalAuthentication-PostgreSQL.git", from: "3.0.0"),
    ],
    targets: [
        
        .target(
            name: "BackendSwift",
            dependencies: ["PerfectHTTPServer",
                           "PerfectPostgreSQL",
                           "PostgresStORM",
                           "PerfectSession",
                           "PerfectSessionPostgreSQL",
                           "Perfect-LocalAuthentication-PostgreSQL"
        ]),
        .testTarget(
            name: "BackendSwiftTests",
            dependencies: ["BackendSwift"]),
    ]
)
