//
//  AccountHandler.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/13.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer
import PerfectLocalAuthentication
import PerfectSessionPostgreSQL
import PerfectSession
import PerfectCrypto
import PerfectLib


class AccountHandler{
    static let register:(
        get:(HTTPRequest, HTTPResponse) -> Void,
        post:(HTTPRequest, HTTPResponse) -> Void) = (registerGet, registerPost)
    
}

// MARK: - 註冊
extension AccountHandler{
    struct RegisterRequest:Codable {
        let username:String
        let password:String
    }
    
    static private func registerPost(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
            guard let param = try Tools.paramsCheck(
                type: RegisterRequest.self,
                request: request, response: response) else{return}
            
            if try self.isUseridHasBeenUsed(username: param.username){
                try response.setReturnData(res: AccountResponseType.usernameHasBeenUsed())
                return
            }
            
            let account = Account()
            account.makeID()
            account.username = param.username
            account.makePassword(param.password)
            account.usertype = .standard
            try account.create()
            let profile = UserProfile()
            profile.userId = account.id
            try profile.create()
        }
    }
    
    static private func registerGet(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
//            try response.setReturnData(res: GenerialResponse.SimpleSuccess())
        }
    }
    
    /// 是否帳號已被使用過
    static private func isUseridHasBeenUsed(username:String)throws -> Bool{
        let account = Account()
        try account.select(whereclause: "username = $1",
                       params: [username],
                       orderby: ["id"])
        
        if account.rows().count != 0{
            return true
        }
        return false
    }
}

// MARK: - 登入
extension AccountHandler{
    struct LoginRequest:Codable {
        let username:String
        let password:String
    }
    static func login(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
            guard let param = try Tools.paramsCheck(
            type: LoginRequest.self,
            request: request, response: response) else{return}
            
            guard let acc = try? Account.login(param.username, param.password) else{
                try response.setReturnData(res: AccountResponseType.loginError())
                return
            }
            request.session!.userid = acc.id
            
            let message = "wellcome \(acc.username)"
            response.setBody(string: message)
            return
        }
    }
    
    static func logout(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response){
            if let _ = request.session?.token {
                logoutProcess(request: request, response: response)
            }else{
                try response.setReturnData(res: AccountResponseType.userNotLoginError())
            }
        }
    }
    static private func logoutProcess(request: HTTPRequest, response: HTTPResponse){
        PostgresSessions().destroy(request, response)
        request.session = PerfectSession()
        response.request.session = PerfectSession()
    }
    
}

// MARK: - 個人資料
extension AccountHandler{
    static func uplordHeadShot(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response){
            guard userIslogin(request: request, response: response) else {return}
            guard let uploads = request.postFileUploads, uploads.count > 0 else {
                try response.setReturnData(res: GenerialResponse.MissingParameters())
                return
            }
            //"image/png"
            guard let fileOutline = uploads.first(where: {$0.fieldName == "headShot"}) else{
                try response.setReturnData(res: GenerialResponse.MissingParameters())
                return
            }
            let legallyType = ["image/png"] // "image/jpeg"
            guard let _ = legallyType.first(where: {$0 == fileOutline.contentType}) else{
                try response.setReturnData(res: GenerialResponse.FileTypeError())
                return
            }
            
            guard fileOutline.fileSize <= 5000000 else {    // 5MB
                try response.setReturnData(res: GenerialResponse.FileSizeError())
                return
            }
            
            let file = File(fileOutline.tmpFileName)
            let userId = request.session!.userid
            let headShotDir = try FilePathManager.headShotDir()
            let fileName = userId + ".png"
            let path = headShotDir.path + fileName
            let _ = try file.moveTo(path: path, overWrite: true)
            
        }
    }
    static func getHeadShot(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response){
            guard let userId = request.param(name: "userId") else{
                try response.setReturnData(res: GenerialResponse.MissingParameters())
                return
            }
            let headShotDir = try! FilePathManager.headShotDir().path
            let fileName = userId + ".png"
            request.path = fileName
            let path = String(headShotDir[..<headShotDir.index(headShotDir.endIndex, offsetBy: -1)])
            let handler = StaticFileHandler(documentRoot: path)
            handler.handleRequest(request: request, response: response)
        }
    }
}

// MARK: - 輔助
extension AccountHandler{
    static func userIslogin(request: HTTPRequest, response: HTTPResponse) -> Bool{
        if request.session?.userid == nil || request.session!.userid.isEmpty{
            try! response.setReturnData(res: AccountResponseType.userNotLoginError())
            response.completed()
            return false
        }
        return true
    }
}
