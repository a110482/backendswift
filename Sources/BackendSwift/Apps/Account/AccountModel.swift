//
//  AccountModel.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/17.
//

import Foundation
import PostgresStORM
import StORM
import PerfectLocalAuthentication

class UserProfile:PostgresStORM{
    typealias selfType = UserProfile
    var userId:String = ""
    var headShotPath:String = ""
    
    override open func table() -> String {
        return "user_profile"
    }
    
    override func to(_ this: StORMRow) {
        userId = this.data["userid"] as! String
        headShotPath = this.data["headshotpath"] as! String
    }
     
    func rows() -> [selfType] {
        var rows = [selfType]()
        for i in 0..<self.results.rows.count {
            let row = selfType()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    override func setup(_ str: String = "") throws {
        guard try !isTableExist() else {
            return
        }
        try super.setup(str)
        try? self.addForeignKey(
            column: "userId",
            to: Account(),
            tablePrimaryKey: "id",
            onDelete: .CASCADE ,
            unique: true)
    }
}


