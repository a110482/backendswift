//
//  AccountResponseType.swift
//  BackendSwift
//
//  Created by Elijah on 2019/11/14.
//

import Foundation

// error code start with "2xxx"
enum AccountResponseType {
    
    /// 帳號已被註冊
    struct usernameHasBeenUsed:ReturnDataType {
        typealias PayLord = String
        var payload: String? = nil
        var statusCode:Int = 2000
        var message:String = "userid has been used"
    }
    
    /// 帳號或密碼錯誤
    struct loginError:ReturnDataType {
        typealias PayLord = String
        var payload: String? = nil
        var statusCode:Int = 2010
        var message:String = "username or password incorrect"
    }
    
    /// 使用者未登入
    struct userNotLoginError:ReturnDataType {
        typealias PayLord = String
        var payload: String? = nil
        var statusCode:Int = 2011
        var message:String = "user not longin"
    }
}
