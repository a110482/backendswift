//
//  AccountRoutes.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/13.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer

struct AccountRoutes{
    static var routes:Routes{
        var routes = Routes(baseUri: "/account")
        routes.add(method: .get, uri: "/register", handler: AccountHandler.register.get)
        routes.add(method: .post, uri: "/register", handler: AccountHandler.register.post)
        routes.add(method: .post, uri: "/login", handler: AccountHandler.login)
        routes.add(method: .get, uri: "/logout", handler: AccountHandler.logout)
        routes.add(method: .post, uri: "/uplordHeadShot", handler: AccountHandler.uplordHeadShot)
        routes.add(method: .post, uri: "/getHeadShot", handler: AccountHandler.getHeadShot)
        return routes
    }
}
