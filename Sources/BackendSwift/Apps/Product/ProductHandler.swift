//
//  ProductHandler.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/27.
//

import Foundation
import PerfectHTTP
import PerfectLocalAuthentication

import StORM
import PerfectPostgreSQL

class ProductHandler{
    static func testc(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
            guard AccountHandler.userIslogin(request: request, response: response) else {return}
            let userid = request.session!.userid
            let obj = Product()
            obj.productName = "p1"
            obj.ownerUserId = userid
            obj.price = 1.2345678
            try obj.create()
        }
    }
    static func testr(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
            guard AccountHandler.userIslogin(request: request, response: response) else {return}
            let userid = request.session!.userid
            let obj = Product()
            let stm = "select * from \(obj.table()) where ownerUserId = '\(userid)' limit 1;"
            let res = try obj.sql(stm, params: [])
            let resp = obj.parseRows(res)
            obj.to(resp[0])
            
            var ret = GenerialResponse.SimpleSuccess()
            ret.message = "productName: \(obj.productName)"
            try response.setReturnData(res: ret)
        }
    }
}
