//
//  ProductModel.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/27.
//

import Foundation
import PostgresStORM
import StORM
import PerfectLocalAuthentication

class ProductTag:PostgresStORM{
    typealias selfType = ProductTag
    var id = 0
    var title:String = ""
    
    override open func table() -> String {
        return "productTag"
    }
    override func to(_ this: StORMRow) {
        id = this.data["id"] as! Int
        title = this.data["title"] as! String
    }
    func rows() -> [selfType] {
        var rows = [selfType]()
        for i in 0..<self.results.rows.count {
            let row = selfType()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    override func setup(_ str: String = "") throws {
        guard try !isTableExist() else {
            return
        }
        try super.setup(str)
        try uniqueColumn(column: "title")
    }
}

class Product:PostgresStORM{
    typealias selfType = Product
    enum PruductStatus:Int{
        case preparing
        case OnSell
        case Suspension
        static func from(_ i:Int) -> PruductStatus{
            return PruductStatus(rawValue:i)!
        }
    }
    enum ProductType:Int{
        case Intact
        case Decorate
        case Meterial
        static func from(_ i:Int) -> ProductType{
            return ProductType(rawValue:i)!
        }
    }
    var id:Int = 0
    var productName:String = ""
    var price:Float = 0
    var ownerUserId:String = ""
    var feeSplitting:Float = 0
    var pruductStatus:PruductStatus = .preparing
    var createDate:Date = Date()
    var productType:ProductType = .Intact
    
    override open func table() -> String {
        return "product"
    }
    
    override func to(_ this: StORMRow) {
        id = this.data["id"] as! Int
        productName = this.data["productname"] as! String
        price = this.data["price"] as! Float
        ownerUserId = this.data["owneruserid"] as! String
        feeSplitting = this.data["feesplitting"] as! Float
        pruductStatus = PruductStatus.from(this.data["pruductstatus"] as! Int)
        createDate = toDate(this.data["createdate"]!)
        productType = ProductType.from(this.data["producttype"] as! Int)
    }

    override func modifyValue(_ v: Any, forKey k: String) -> Any {
        switch k {
        case "pruductStatus":
            return (v as! PruductStatus).rawValue as Any
        case "productType":
            return (v as! ProductType).rawValue as Any
        default:
            return v
        }
    }
    func rows() -> [selfType] {
        var rows = [selfType]()
        for i in 0..<self.results.rows.count {
            let row = selfType()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    override func setup(_ str: String = "") throws {
        guard try !isTableExist() else {
            return
        }
        try super.setup(str)
        try self.addForeignKey(
            column: "ownerUserId",
            to: Account(),
            tablePrimaryKey: "id",
            onDelete: .CASCADE ,
            unique: true)
        try toDate(columns: ["createdate"])
        try toDouble(columns: ["price", "feesplitting"])
        
    }
    private func toDate(_ v: Any) -> Date{
        let timeStr:String = v as! String
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSS"
        formatter.timeZone = TimeZone(identifier: "UTC")
        let d:Date = formatter.date(from: timeStr)!
        return d
    }
}

// many to many relationship from "Product" to "ProductTag"
class ProductTagList:PostgresStORM{
    typealias selfType = ProductTag
    var productId:Int = 0
    var productTagId:Int = 0
    
    override open func table() -> String {
        return "ProductTagList"
    }
    override func to(_ this: StORMRow) {
        productId = this.data["productid"] as! Int
        productTagId = this.data["producttagid"] as! Int
    }
    func rows() -> [selfType] {
        var rows = [selfType]()
        for i in 0..<self.results.rows.count {
            let row = selfType()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    override func setup(_ str: String = "") throws {
        guard try !isTableExist() else {
            return
        }
        try super.setup(str)
        try sql("alter table producttaglist drop constraint producttaglist_key;", params: [])
        try sql("alter table producttaglist add primary key (productid, producttagid);", params: [])
        //try addForeignKey(column: "productId", to: Product(), tablePrimaryKey: "id")
        //try addForeignKey(column: "productTagId", to: ProductTag(), tablePrimaryKey: "id")
    }
}
