//
//  ProductRoutes.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/27.
//

import Foundation
import PerfectHTTP
//import PerfectHTTPServer

struct ProductRoutes{
    static var routes:Routes{
        var routes = Routes(baseUri: "/product")
        routes.add(uri: "testc", handler: ProductHandler.testc)
        routes.add(uri: "testr", handler: ProductHandler.testr)
        return routes
    }
}
