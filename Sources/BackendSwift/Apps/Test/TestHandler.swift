//
//  TestController.swift
//  BackendSwift
//
//  Created by elijah on 2019/10/30.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer
import PerfectPostgreSQL
import StORM
import PerfectLib

// 資料庫
class TestHandler{
    static func dbTest(request: HTTPRequest, response: HTTPResponse){
        let p = PGConnection()
        let status = p.connectdb("host=localhost dbname=dud_database port=5000 user=dud_db_user password=joanna31")
        p.close()
        response.setBody(string: "database status: \(status)")
        response.completed()
    }
    
    static func setup(request: HTTPRequest, response: HTTPResponse){
        //let objs = [Author(), Book()]
        let objs = [TimeTest()]
        Tools.standerTryErrorHandler(response: response) {
            for obj in objs{
                try obj.setup()
            }
        }
    }
    
    static func truncate(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
            try Author().sql("TRUNCATE $1, $2", params: ["author", "book"])
        }
    }
    
    static func add(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
            let param = request.params()
            let auth = param.first(){$0.0 == "auth"}!
            
            // 第一次建立
            var authObj = Author()
            func select() throws{
                try authObj.select(whereclause: "name = $1", params: [auth.1], orderby: ["id"])
            }
            try select()
            if authObj.rows().count == 0{
                authObj.name = auth.1
                try! authObj.save()
                try select()
            }

            authObj = authObj.rows()[0]
            
            
            let book = param.first(){$0.0 == "book"}!
            let bookObj = Book()
            bookObj.title = book.1
            bookObj.author = authObj.id
            try! bookObj.save()
        }
    }
    
    static func post(request: HTTPRequest, response: HTTPResponse){
        let param = request.params()
        print(param)
        response.setBody(string: "success")
        response.completed()
    }
    
    static func select(request: HTTPRequest, response: HTTPResponse){
        Tools.standerTryErrorHandler(response: response) {
            let _ = "select * from book as b JOIN author as a ON a.id = b.author where a.name = 'elijah';"

        }
    }
    
    static func test(request: HTTPRequest, response: HTTPResponse){
        print("test")
        Tools.standerTryErrorHandler(response: response) {
            //let obj = Product()
            // obj.
        }
    }
}

extension TestHandler{
    static func uplord(request: HTTPRequest, response: HTTPResponse){
        print("uplord...")
        // 找目錄
        let fileDir = Dir(Dir.workingDir.path + "files")
        do {
            try fileDir.create()
        } catch {
            print(error)
        }
        
        if let uploads = request.postFileUploads, uploads.count > 0 {
            // 创建一个字典数组用于检查已经上载的内容
            var ary = [[String:Any]]()
         
            for upload in uploads {
                ary.append([
                    "fieldName": upload.fieldName,  //字段名
                    "contentType": upload.contentType, //文件内容类型
                    "fileName": upload.fileName,    //文件名
                    "fileSize": upload.fileSize,    //文件尺寸
                    "tmpFileName": upload.tmpFileName   //上载后的临时文件名
                    ])
                print("fieldName: \(upload.fieldName)")
                print("tmpFileName: \(upload.tmpFileName)")
                // 存擋
                let thisFile = File(upload.tmpFileName)
                do {
                    let _ = try thisFile.moveTo(path: fileDir.path + upload.fileName, overWrite: true)
                } catch {
                    print(error)
                }
            }
        }
        try! response.setReturnData(res: GenerialResponse.SimpleSuccess())
        response.completed()
    }
}
