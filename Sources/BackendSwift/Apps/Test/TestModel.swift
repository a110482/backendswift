//
//  TestModel.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/3.
//

import Foundation
import PostgresStORM
import StORM


class Author:PostgresStORM{
    typealias selfType = Author
    var id:Int = 0
    var name:String = ""
    
    override open func table() -> String {
        return "author"
    }
    
    override func to(_ this: StORMRow) {
        id = this.data["id"] as! Int
        name = this.data["name"] as! String
    }
     
    func rows() -> [selfType] {
        var rows = [selfType]()
        for i in 0..<self.results.rows.count {
            let row = selfType()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    override func setup(_ str: String = "") throws {
        try super.setup(str)
    }
}


class Book:PostgresStORM{
    typealias selfType = Book
    var id:Int = 0
    var title:String = ""
    var author:Int = 0
    
    override open func table() -> String {
        return "book"
    }
    
    override func to(_ this: StORMRow) {
        id = this.data["id"] as! Int
        title = this.data["name"] as! String
        author = this.data["age"] as! Int
    }
     
    func rows() -> [selfType] {
        var rows = [selfType]()
        for i in 0..<self.results.rows.count {
            let row = selfType()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    override func setup(_ str: String = "") throws {
        try super.setup(str)
        try self.addForeignKey(column: "author", to: Author(), tablePrimaryKey: "id", unique: false)
    }
}

class TimeTest:PostgresStORM{
    typealias selfType = TimeTest
    var id:Int = 0
    var ts:Date = Date()
    
    override open func table() -> String {
        return "time_test"
    }
    
    override func to(_ this: StORMRow) {
        id = this.data["id"] as! Int
        ts = self.modifyValue(this.data["ts"] as Any, forKey: "ts") as! Date
    }
     
    func rows() -> [selfType] {
        var rows = [selfType]()
        for i in 0..<self.results.rows.count {
            let row = selfType()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    override func modifyValue(_ v: Any, forKey k: String) -> Any {
        if k == "ts"{
            let timeStr:String = v as! String
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSS"
            formatter.timeZone = TimeZone(identifier: "UTC")
            let d:Date = formatter.date(from: timeStr)!
            return d
        }
        return super.modifyValue(v, forKey: k)
    }
    func test()throws{
        print("model test")
        let res = try sql("select to_regclass('sessions');", params: [])
        print(res)
    }
}
