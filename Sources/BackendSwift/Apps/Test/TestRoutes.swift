//
//  TestRoutes.swift
//  BackendSwift
//
//  Created by elijah on 2019/10/31.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer

class TestRoutes{
    static var routes:Routes{
        var testRoutes = Routes(baseUri: "/test")
        testRoutes.add(uris: ["/", "/test"], handler: TestHandler.test)
        testRoutes.add(uri: "/post", handler: TestHandler.post)
        testRoutes.add(uri: "/setup", handler: TestHandler.setup)
        testRoutes.add(uri: "/add", handler: TestHandler.add)
        testRoutes.add(uri: "/select", handler: TestHandler.select)
        testRoutes.add(uri: "/truncate", handler: TestHandler.truncate)
        testRoutes.add(uri: "/uplord", handler: TestHandler.uplord(request:response:))
        return testRoutes
    }
}
