//
//  Extension_ORM.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/9.
//

import Foundation
import PostgresStORM
import StORM
import PerfectSessionPostgreSQL
import PerfectLocalAuthentication

extension PostgresStORM{
    enum ForeignKeyDeleteType:String {
        case RESTRICT = "ON DELETE RESTRICT"
        case CASCADE = "ON DELETE CASCADE"
        case NONE = ""
    }
    
    
    /// addForeignKey
    /// - Parameter colunm: colunm name of ForeignKey  ex.
    /// - Parameter table: parent able
    /// - Parameter tablePrimaryKey: primary key of parent able
    /// - Parameter onDelete: foreign key type
    /// - Parameter unique: if you need one to on field ,select true
    func addForeignKey(column:String,to table:PostgresStORM, tablePrimaryKey:String, onDelete:ForeignKeyDeleteType = .NONE, unique:Bool = false) throws{
        var createStatement = "ALTER TABLE \(self.table()) ADD CONSTRAINT \(column)_fk FOREIGN KEY (\(column)) REFERENCES \(table.table()) (\(tablePrimaryKey)) \(onDelete.rawValue);"
        try sql(createStatement, params: [])
        
        if unique{
            createStatement = "ALTER TABLE \(self.table()) ADD CONSTRAINT \(column)_unique UNIQUE(\(column));"
            try sql(createStatement, params: [])
        }
    }
    /// 讓 column unique
    func uniqueColumn(column:String)throws{
        
        let createStatement = "ALTER TABLE \(self.table()) ADD CONSTRAINT \(column)_unique UNIQUE(\(column));"
        let _ = try sql(createStatement, params: [])
    }
    
    /// 判斷 table 是否存在
    func isTableExist()throws -> Bool{
        let res = try sql("select to_regclass('\(table())');", params: [])
        return !res.fieldIsNull(tupleIndex: 0, fieldIndex: 0)
    }
    
    
    enum PostgresType{
        case float
        case data
        case int
        func typeName() -> String{
            return ""
        }
        func using() -> String{
            return ""
        }
    }
    
    
    /// 轉成 Double
    func toDouble(columns:Array<String>)throws{
        for column in columns{
            let sta = "alter table \(table()) alter COLUMN \(column) type float USING \(column)::double precision;"
            try sql(sta, params: [])
        }
    }
    
    /// 轉成 Date
    func toDate(columns:Array<String>)throws{
        for column in columns{
            let sta = "alter table \(table()) alter COLUMN \(column) type date USING \(column)::date;"
            try sql(sta, params: [])
        }
        
        
    }
    
    
}

