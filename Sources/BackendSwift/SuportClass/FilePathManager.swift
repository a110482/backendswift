//
//  FileManager.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/30.
//

import Foundation
import PerfectLib

class FilePathManager{
    private static let workDir = Dir("~/Documents/DUDProject/BackendSwift/StaticFiles")
    private static let headShot = "headShot/"
    
    // 初始化
    static func setup()throws{
        if !workDir.exists{
            try workDir.create()
        }
        try workDir.setAsWorkingDir()
    }
    
    // 大頭照片
    static func headShotDir()throws -> Dir{
        let headShotDir = Dir("\(workDir.path)\(headShot)")
        if !headShotDir.exists{
            try headShotDir.create()
        }
        return headShotDir
    }
}
