//
//  MainRoutes.swift
//  BackendSwift
//
//  Created by elijah on 2019/10/30.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer

struct MainRoutes{
    static var main:Routes{
        var mainRoutes = Routes()
        mainRoutes.add(TestRoutes.routes)
        mainRoutes.add(AccountRoutes.routes)
        mainRoutes.add(ProductRoutes.routes)
        return mainRoutes
    }
}
