//
//  ErrorTypes.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/13.
//

import Foundation
import PerfectHTTP


extension HTTPResponse{
    /// 直接設置回應內容
    func setReturnData<T:ReturnDataType>(res:T) throws{
        try self.setBody(json: res, skipContentType: false)
    }
}

/// 設置回應格式
/// 預設 statusCode = 1000 代表正常
protocol ReturnDataType:Codable {
    var statusCode:Int { get }
    var message:String { get }
    associatedtype PayLord
    var payload:PayLord? { get set}
}

enum GenerialResponse {
    
    /// 設置成功回應格式
    /// 預設 statusCode = 1000 代表正常
    struct SimpleSuccess:ReturnDataType {
        typealias PayLord = String
        var statusCode:Int = 1000
        var message:String = "success"
        var payload:String? = nil
    }
    
    /// 設置失敗回應格式
    /// 預設 statusCode = 1000 代表正常
    struct UnknowError:ReturnDataType{
        typealias PayLord = String
        var statusCode:Int = 1010
        var message:String = "undefined error"
        var payload:String? = nil
    }
    
    /// 檔案格式不符
    /// 預設 statusCode = 1000 代表正常
    struct FileTypeError:ReturnDataType{
        typealias PayLord = String
        var statusCode:Int = 1011
        var message:String = "file type error"
        var payload:String? = nil
    }
    
    /// 檔案大小不符
    /// 預設 statusCode = 1000 代表正常
    struct FileSizeError:ReturnDataType{
        typealias PayLord = String
        var statusCode:Int = 1012
        var message:String = "file size error"
        var payload:String? = nil
    }
    
    /// 設置成功回應格式
    /// 預設 statusCode = 1000 代表正常
    struct MissingParameters:ReturnDataType {
        typealias PayLord = String
        var statusCode:Int = 1011
        var message:String = "missing parameters"
        var payload:String? = nil
    }
    
    /// debug 專用回應格式
    struct DebugError:ReturnDataType{
        typealias PayLord = String
        var statusCode:Int = 1999
        var message:String
        var payload:String? = nil
    }
}
