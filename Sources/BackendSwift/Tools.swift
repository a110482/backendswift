//
//  Tools.swift
//  BackendSwift
//
//  Created by elijah on 2019/11/3.
//

import Foundation
import PerfectHTTP
import PostgresStORM
import PerfectCrypto
import PerfectLocalAuthentication

class Tools{
    static let models:Array<PostgresStORM> = [
        Account(),
        UserProfile(),
        ProductTag(),
        Product(),
        ProductTagList(),
    ]
    
    /// orm 建立model
    static func setupModels() throws{
        for m in models{
            try? m.setup()
        }
    }
    
    /// 標準錯誤列印處理器
    static func standerTryErrorHandler(response: HTTPResponse, function:() throws->Void){
        do{
            try function()
            if response.bodyBytes.count == 0{
                try? response.setReturnData(res: GenerialResponse.SimpleSuccess())
                response.completed()
            }
            response.completed()
        }catch{
            #if DEBUG
            if case let .dataCorrupted(c) = error as? DecodingError {
                let key = c.codingPath[0].stringValue
                let errorStr = "key: \"\(key)\" " + c.debugDescription
                try? response.setReturnData(res: GenerialResponse.DebugError(message: errorStr))
            }else if case let .keyNotFound(_, c) = error as? DecodingError {
                let errorStr = c.debugDescription
                try? response.setReturnData(res: GenerialResponse.DebugError(message: errorStr))
            }else{
                try? response.setReturnData(res: GenerialResponse.DebugError(message: error.localizedDescription))
            }
            #else
            try? response.setReturnData(res: GenerialResponse.UnknowError())
            #endif
            response.completed()
        }
        
    }
    
    /// 參數檢查
    /// - Parameter params: paramater keys
    /// - Parameter request: request
    /// - Parameter response: response
    /// - Return if no missing params return true
    static func paramsCheck<T:Codable>(type:T.Type, request: HTTPRequest, response: HTTPResponse) throws -> T?{
        let contentType = request.header(.contentType) ?? "application/x-www-form-urlencoded"
        var dataString:String = ""
        
        switch contentType {
        case "application/json":
            let params = request.params()
            dataString = params[0].0
        default:
            let params = request.params()
            dataString = params.reduce("{"){$0 + "\"\($1.0)\":\"\($1.1)\","} + "}"
        }
        
        guard let data:Data = dataString.data(using: .utf8) else {throw "params paser error"}
        
        if let json = try? JSONDecoder().decode(type, from: data){return json}
        
        #if DEBUG
        let _ = try JSONDecoder().decode(type, from: data)
        #else
        try response.setReturnData(res: GenerialResponse.MissingParameters())
        response.completed()
        #endif
        
        return nil
    }
}



