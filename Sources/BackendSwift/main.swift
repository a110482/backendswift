import PerfectHTTP
import PerfectHTTPServer
import PostgresStORM
import PerfectSession
import PerfectSessionPostgreSQL


// =============================
// ========== database ==========
PostgresConnector.host        = "localhost"
PostgresConnector.username    = "dud_db_user"
PostgresConnector.password    = "joanna31"
PostgresConnector.database    = "dud_database"
PostgresConnector.port        = 5000
// =============================


// =============================
// ========== session ==========
SessionConfig.name = "session"
SessionConfig.idle = 60*60*8*30 // 30 days

// Optional
SessionConfig.cookieDomain = "localhost"
SessionConfig.IPAddressLock = true
SessionConfig.userAgentLock = true
SessionConfig.CSRF.checkState = true
PostgresSessionConnector.host = PostgresConnector.host
PostgresSessionConnector.username = PostgresConnector.username
PostgresSessionConnector.password = PostgresConnector.password
PostgresSessionConnector.database = PostgresConnector.database
PostgresSessionConnector.table = "sessions"
PostgresSessionConnector.port = PostgresConnector.port
let sessionDriver = SessionPostgresDriver()
// cors
SessionConfig.CORS.enabled = false
SessionConfig.CORS.acceptableHostnames.append("http://www.test-cors.org")
//SessionConfig.CORS.acceptableHostnames.append("*.test-cors.org")
SessionConfig.CORS.maxAge = 60
// =============================



// 注册您自己的路由和请求／响应句柄
var routes = MainRoutes.main

do {
    try Tools.setupModels()
    try FilePathManager.setup()
    let server = HTTPServer()
    server.serverName = "testServer"
    server.serverPort = 8080
    server.addRoutes(routes)
    server.setRequestFilters([sessionDriver.requestFilter])
    server.setResponseFilters([sessionDriver.responseFilter])
    try server.start()
} catch {
    fatalError("\(error)") // fatal error launching one of the servers
}
