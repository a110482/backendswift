import XCTest

import BackendSwiftTests

var tests = [XCTestCaseEntry]()
tests += BackendSwiftTests.allTests()
XCTMain(tests)
